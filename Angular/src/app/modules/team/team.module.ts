import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TeamComponent } from "./components/team/team.component";
import { TeamListComponent } from "./components/team-list/team-list.component";
import { TeamEditComponent } from "./components/team-edit/team-edit.component";
import { SharedModule } from "src/app/shared/shared.module";

@NgModule({
  declarations: [TeamComponent, TeamListComponent, TeamEditComponent],
  imports: [CommonModule, SharedModule],
})
export class TeamModule {}
