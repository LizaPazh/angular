import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Team } from "src/app/models/team";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { TeamService } from "src/app/services/team.service";
import { TeamEditComponent } from "../team-edit/team-edit.component";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

@Component({
  selector: "app-team",
  templateUrl: "./team.component.html",
  styleUrls: ["./team.component.css"],
})
export class TeamComponent implements OnInit {
  @Input() public team: Team;
  private unsubscribe$ = new Subject<void>();
  @Output() teamDeleted: EventEmitter<any> = new EventEmitter();
  constructor(
    private modalService: NgbModal,
    private teamService: TeamService
  ) {}

  ngOnInit(): void {}
  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
  public openEditTeam(id: number) {
    const modalRef = this.modalService.open(TeamEditComponent);
    modalRef.componentInstance.id = id;

    modalRef.result
      .then((result) => {
        if (result) {
          this.team = result;
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }
  public deleteTeam(id: number) {
    var task = this.teamService
      .deleteTeam(id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((res) => {
        this.teamDeleted.emit();
      });
  }
}
