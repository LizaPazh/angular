import { Component, OnInit, Input } from "@angular/core";
import { Team } from "src/app/models/team";
import { TeamService } from "src/app/services/team.service";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

@Component({
  selector: "app-team-edit",
  templateUrl: "./team-edit.component.html",
  styleUrls: ["./team-edit.component.css"],
})
export class TeamEditComponent implements OnInit {
  @Input() id: number;
  private unsubscribe$ = new Subject<void>();
  team: Team;
  constructor(
    private teamService: TeamService,
    private activeModal: NgbActiveModal
  ) {}

  ngOnInit(): void {
    this.teamService
      .getTeam(this.id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((result) => {
        this.team = result;
      });
  }
  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  public saveNewInfo() {
    this.teamService
      .updateTeam(this.team)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((result) => {
        this.team = result;
      });
    this.activeModal.close(this.team);
  }
  close() {
    this.activeModal.close();
  }
}
