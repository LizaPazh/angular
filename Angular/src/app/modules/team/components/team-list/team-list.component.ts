import { Component, OnInit } from "@angular/core";
import { Team } from "src/app/models/team";
import { TeamService } from "src/app/services/team.service";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

@Component({
  selector: "app-team-list",
  templateUrl: "./team-list.component.html",
  styleUrls: ["./team-list.component.css"],
})
export class TeamListComponent implements OnInit {
  public teams: Team[];
  private unsubscribe$ = new Subject<void>();
  constructor(private teamService: TeamService) {}

  ngOnInit(): void {
    this.teamService
      .getTeams()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((result) => (this.teams = result));
  }
  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
  onTeamDeleted(team) {
    var index = this.teams.findIndex((t) => t === team);
    if (index !== -1) {
      this.teams.splice(index, 1);
    }
  }
}
