import { Component, OnInit, Input } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { User } from "src/app/models/user";
import { UserService } from "src/app/services/user.service";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

@Component({
  selector: "app-user-edit",
  templateUrl: "./user-edit.component.html",
  styleUrls: ["./user-edit.component.css"],
})
export class UserEditComponent implements OnInit {
  @Input() id: number;
  user: User;
  private unsubscribe$ = new Subject<void>();
  constructor(
    private userService: UserService,
    private activeModal: NgbActiveModal
  ) {}

  ngOnInit(): void {
    this.userService
      .getUser(this.id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((result) => {
        this.user = result;
      });
  }
  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
  public onSubmit() {
    this.userService
      .updateUser(this.user)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((result) => {
        this.user = result;
      });
    this.activeModal.close(this.user);
  }
  close() {
    this.activeModal.close();
  }
}
