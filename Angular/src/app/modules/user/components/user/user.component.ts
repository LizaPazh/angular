import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild,
} from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { User } from "src/app/models/user";
import { UserService } from "src/app/services/user.service";
import { UserEditComponent } from "../user-edit/user-edit.component";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

@Component({
  selector: "app-user",
  templateUrl: "./user.component.html",
  styleUrls: ["./user.component.css"],
})
export class UserComponent implements OnInit {
  @Input() public user: User;
  @Output() userDeleted: EventEmitter<any> = new EventEmitter();

  private unsubscribe$ = new Subject<void>();
  constructor(
    private modalService: NgbModal,
    private userService: UserService
  ) {}

  ngOnInit(): void {}
  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
  public openEditUser(id: number) {
    const modalRef = this.modalService.open(UserEditComponent);
    modalRef.componentInstance.id = id;

    modalRef.result
      .then((result) => {
        if (result) {
          this.user = result;
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }
  public deleteUser(id: number) {
    var task = this.userService
      .deleteUser(id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((res) => {
        this.userDeleted.emit();
      });
  }
}
