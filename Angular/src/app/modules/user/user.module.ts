import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { UserComponent } from "./components/user/user.component";
import { UserListComponent } from "./components/user-list/user-list.component";
import { UserEditComponent } from "./components/user-edit/user-edit.component";
import { SharedModule } from "src/app/shared/shared.module";

@NgModule({
  declarations: [UserComponent, UserListComponent, UserEditComponent],
  imports: [CommonModule, SharedModule],
})
export class UserModule {}
