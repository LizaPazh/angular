import { Component, OnInit } from "@angular/core";
import { ProjectService } from "src/app/services/project.service";
import { Project } from "src/app/models/project";
import { takeUntil } from "rxjs/operators";
import { Subject } from "rxjs";

@Component({
  selector: "app-project-list",
  templateUrl: "./project-list.component.html",
  styleUrls: ["./project-list.component.css"],
})
export class ProjectListComponent implements OnInit {
  public projects: Project[];
  private unsubscribe$ = new Subject<void>();
  constructor(private projectService: ProjectService) {}

  ngOnInit(): void {
    this.projectService
      .getProjects()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((result) => (this.projects = result));
  }
  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
  onProjectDeleted(project) {
    var index = this.projects.findIndex((p) => p === project);
    if (index !== -1) {
      this.projects.splice(index, 1);
    }
  }
}
