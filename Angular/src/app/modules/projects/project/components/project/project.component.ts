import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Project } from "src/app/models/project";
import { ProjectEditComponent } from "../project-edit/project-edit.component";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ProjectService } from "src/app/services/project.service";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

@Component({
  selector: "app-project",
  templateUrl: "./project.component.html",
  styleUrls: ["./project.component.css"],
})
export class ProjectComponent implements OnInit {
  private unsubscribe$ = new Subject<void>();
  @Input() public project: Project;
  @Output() projectDeleted: EventEmitter<any> = new EventEmitter();
  constructor(
    private modalService: NgbModal,
    private projectService: ProjectService
  ) {}

  ngOnInit(): void {}
  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
  public openEditProject(id: number) {
    const modalRef = this.modalService.open(ProjectEditComponent);
    modalRef.componentInstance.id = id;

    modalRef.result
      .then((result) => {
        if (result) {
          this.project = result;
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }
  public deleteProject(id: number) {
    var task = this.projectService
      .deleteProject(id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((res) => {
        this.projectDeleted.emit();
      });
  }
}
