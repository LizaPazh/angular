import { Component, OnInit, Input } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { Project } from "src/app/models/project";
import { ProjectService } from "src/app/services/project.service";
import { NgForm } from "@angular/forms";
import { takeUntil } from "rxjs/operators";
import { Subject } from "rxjs";

@Component({
  selector: "app-project-edit",
  templateUrl: "./project-edit.component.html",
  styleUrls: ["./project-edit.component.css"],
})
export class ProjectEditComponent implements OnInit {
  @Input() id: number;
  private unsubscribe$ = new Subject<void>();
  project: Project;
  constructor(
    private projectService: ProjectService,
    private activeModal: NgbActiveModal
  ) {}

  ngOnInit(): void {
    this.projectService
      .getProject(this.id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((result) => {
        this.project = result;
      });
  }
  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
  public saveNewInfo() {
    this.projectService
      .updateProject(this.project)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((result) => {
        this.project = result;
      });
    this.activeModal.close(this.project);
  }
  close() {
    this.activeModal.close();
  }
}
