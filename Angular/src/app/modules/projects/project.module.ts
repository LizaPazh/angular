import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ProjectComponent } from "./project/components/project/project.component";
import { ProjectEditComponent } from "./project/components/project-edit/project-edit.component";
import { ProjectListComponent } from "./project/components/project-list/project-list.component";
import { SharedModule } from "src/app/shared/shared.module";

@NgModule({
  declarations: [ProjectComponent, ProjectEditComponent, ProjectListComponent],
  imports: [CommonModule, SharedModule],
})
export class ProjectModule {}
