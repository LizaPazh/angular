import { Directive, Input, ElementRef } from "@angular/core";

@Directive({
  selector: "[status]",
})
export class StatusDirective {
  @Input("status") state: number;
  constructor(private el: ElementRef) {}
  ngOnInit() {
    if (this.state === 0) {
      this.el.nativeElement.style.backgroundColor = "yellow";
    } else if (this.state === 1) {
      this.el.nativeElement.style.backgroundColor = "blue";
    } else if (this.state === 2) {
      this.el.nativeElement.style.backgroundColor = "green";
    } else if (this.state === 3) {
      this.el.nativeElement.style.backgroundColor = "red";
    } else {
      this.el.nativeElement.style.backgroundColor = "white";
    }
  }
}
