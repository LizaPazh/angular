import { Component, OnInit, Input } from "@angular/core";
import { Task, TaskState } from "src/app/models/task";
import { TaskService } from "src/app/services/task.service";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { NgForm } from "@angular/forms";
import { takeUntil } from "rxjs/operators";
import { Subject } from "rxjs";

@Component({
  selector: "app-task-edit",
  templateUrl: "./task-edit.component.html",
  styleUrls: ["./task-edit.component.css"],
})
export class TaskEditComponent implements OnInit {
  @Input() id: number;
  private unsubscribe$ = new Subject<void>();
  task: Task;
  constructor(
    private taskService: TaskService,
    private activeModal: NgbActiveModal
  ) {}

  ngOnInit(): void {
    this.taskService
      .getTask(this.id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((result) => {
        this.task = result;
      });
  }
  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  public saveNewInfo() {
    this.taskService
      .updateTask(this.task)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((result) => {
        this.task = result;
      });
    this.activeModal.close(this.task);
  }
  close() {
    this.activeModal.close();
  }
}
