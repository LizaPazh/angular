import { Component, OnInit } from "@angular/core";
import { Task } from "src/app/models/task";
import { TaskService } from "src/app/services/task.service";
import { takeUntil } from "rxjs/operators";
import { Subject } from "rxjs";

@Component({
  selector: "app-task-list",
  templateUrl: "./task-list.component.html",
  styleUrls: ["./task-list.component.css"],
})
export class TaskListComponent implements OnInit {
  public tasks: Task[];
  private unsubscribe$ = new Subject<void>();
  constructor(private taskService: TaskService) {}

  ngOnInit(): void {
    this.taskService
      .getTasks()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((result) => (this.tasks = result));
  }
  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
  onTaskDeleted(task) {
    var index = this.tasks.findIndex((t) => t === task);
    if (index !== -1) {
      this.tasks.splice(index, 1);
    }
  }
}
