import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Task, TaskState } from "src/app/models/task";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { TaskEditComponent } from "../task-edit/task-edit.component";
import { TaskService } from "src/app/services/task.service";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

@Component({
  selector: "app-task",
  templateUrl: "./task.component.html",
  styleUrls: ["./task.component.css"],
})
export class TaskComponent implements OnInit {
  public taskState: typeof TaskState = TaskState;
  private unsubscribe$ = new Subject<void>();
  @Input() public task: Task;
  @Output() taskDeleted: EventEmitter<any> = new EventEmitter();
  constructor(
    private modalService: NgbModal,
    private taskService: TaskService
  ) {}

  ngOnInit(): void {}
  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
  public openEditTask(id: number) {
    const modalRef = this.modalService.open(TaskEditComponent);
    modalRef.componentInstance.id = id;

    modalRef.result
      .then((result) => {
        if (result) {
          this.task = result;
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }
  public deleteTask(id: number) {
    var task = this.taskService
      .deleteTask(id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((res) => {
        this.taskDeleted.emit();
      });
  }
}
