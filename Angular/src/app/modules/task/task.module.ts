import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TaskComponent } from "./components/task/task.component";
import { TaskListComponent } from "./components/task-list/task-list.component";
import { TaskEditComponent } from "./components/task-edit/task-edit.component";
import { SharedModule } from "src/app/shared/shared.module";
import { StatusDirective } from './status.directive';

@NgModule({
  declarations: [TaskComponent, TaskListComponent, TaskEditComponent, StatusDirective],
  imports: [CommonModule, SharedModule],
})
export class TaskModule {}
