import { Injectable } from "@angular/core";
import {CanDeactivate} from "@angular/router";
import { FormGroup } from "@angular/forms";

export interface FormComponent {
  form: FormGroup;
}

@Injectable({
  providedIn: "root",
})
export class CanCloseGuard implements CanDeactivate<FormComponent> {
  canDeactivate(component: FormComponent) {
    if (component.form.dirty) {
      return confirm(
        "The form has not been submitted yet, do you really want to leave page?"
      );
    }
    return true;
  }
}
