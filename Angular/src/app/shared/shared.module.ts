import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatCardModule } from "@angular/material/card";
import { LOCALE_ID } from "@angular/core";
import { MyDatePipe } from "./my-date.pipe";
import { registerLocaleData } from "@angular/common";
import localeUa from "@angular/common/locales/uk";
import { CanCloseGuard } from "./can-close.guard";

registerLocaleData(localeUa);

@NgModule({
  declarations: [MyDatePipe],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
  ],
  exports: [
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    FormsModule,
    ReactiveFormsModule,
    MyDatePipe,
  ],
  providers: [{ provide: LOCALE_ID, useValue: "uk" }, CanCloseGuard],
})
export class SharedModule {}
