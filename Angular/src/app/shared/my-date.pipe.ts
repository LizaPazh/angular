import { Pipe, PipeTransform } from "@angular/core";
import { DatePipe } from "@angular/common";
import { TranslateService } from "@ngx-translate/core";

@Pipe({
  name: "myDate",
})
export class MyDatePipe extends DatePipe implements PipeTransform {
  transform(value: any, args?: any): any {
    return super.transform(value, "d MMMM y");
  }
}
