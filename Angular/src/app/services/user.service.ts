import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { User } from "../models/user";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root",
})
export class UserService {
  public baseUrl: string = environment.apiUrl + `users`;
  constructor(private http: HttpClient) {}
  getUsers() {
    return this.http.get<User[]>(this.baseUrl);
  }
  getUser(id: number) {
    return this.http.get<User>(this.baseUrl + `/` + id);
  }
  updateUser(user: User) {
    return this.http.put<User>(this.baseUrl, user);
  }
  deleteUser(id: number) {
    return this.http.delete<User>(this.baseUrl + `/` + id);
  }
}
