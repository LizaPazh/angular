import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Team } from "../models/team";
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: "root",
})
export class TeamService {
  public baseUrl: string = environment.apiUrl + `teams`;
  constructor(private http: HttpClient) {}
  getTeams() {
    return this.http.get<Team[]>(this.baseUrl);
  }
  getTeam(id: number) {
    return this.http.get<Team>(this.baseUrl + `/` + id);
  }
  updateTeam(team: Team) {
    return this.http.put<Team>(this.baseUrl, team);
  }
  deleteTeam(id: number) {
    return this.http.delete<Team>(this.baseUrl + `/` + id);
  }
}
