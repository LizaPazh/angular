import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Task } from "../models/task";
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: "root",
})
export class TaskService {
  public baseUrl: string = environment.apiUrl + `tasks`;
  constructor(private http: HttpClient) {}
  getTasks() {
    return this.http.get<Task[]>(this.baseUrl);
  }
  getTask(id: number) {
    return this.http.get<Task>(this.baseUrl + `/` + id);
  }
  updateTask(task: Task) {
    return this.http.put<Task>(this.baseUrl, task);
  }
  deleteTask(id: number) {
    return this.http.delete<Task>(this.baseUrl + `/` + id);
  }
}
