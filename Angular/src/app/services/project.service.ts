import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Project } from "../models/project";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root",
})
export class ProjectService {
  public baseUrl: string = environment.apiUrl + `projects`;
  constructor(private http: HttpClient) {}
  getProjects() {
    return this.http.get<Project[]>(this.baseUrl);
  }
  getProject(id: number) {
    return this.http.get<Project>(this.baseUrl + `/` + id);
  }
  updateProject(project: Project) {
    return this.http.put<Project>(this.baseUrl, project);
  }
  deleteProject(id: number) {
    return this.http.delete<Project>(this.baseUrl + `/` + id);
  }
}
