import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "./shared/home/home.component";
import { ProjectListComponent } from "./modules/projects/project/components/project-list/project-list.component";
import { UserListComponent } from "./modules/user/components/user-list/user-list.component";
import { TaskListComponent } from "./modules/task/components/task-list/task-list.component";
import { TeamListComponent } from "./modules/team/components/team-list/team-list.component";

const routes: Routes = [
  { path: "", component: HomeComponent },
  { path: "projects", component: ProjectListComponent },
  {
    path: "users",
    component: UserListComponent,
  },
  { path: "tasks", component: TaskListComponent },
  {
    path: "teams",
    component: TeamListComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
