import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HttpClientModule } from "@angular/common/http";

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { NoopAnimationsModule } from "@angular/platform-browser/animations";
import { HomeComponent } from "./shared/home/home.component";
import { ProjectModule } from "./modules/projects/project.module";
import { TaskModule } from "./modules/task/task.module";
import { SharedModule } from "./shared/shared.module";
import { TeamModule } from "./modules/team/team.module";
import { UserModule } from "./modules/user/user.module";

@NgModule({
  declarations: [AppComponent, HomeComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ProjectModule,
    TaskModule,
    TeamModule,
    UserModule,
    NgbModule,
    SharedModule,
    NoopAnimationsModule,
  ],
  exports: [],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
