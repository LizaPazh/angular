export class Task {
  id: number;
  name: string;
  description: string;
  createdAt: Date;
  finishedAt: Date;
  state: TaskState;
}
export enum TaskState {
  Created = 0,
  Started = 1,
  Finished = 2,
  Canceled = 3,
}
