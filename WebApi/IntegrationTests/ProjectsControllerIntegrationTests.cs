﻿using BLL.DTOs;
using DAL.Models;
using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using Project_Structure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace IntegrationTests
{
    public class ProjectsControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;
        private const string BaseUrl = "http://localhost:44392/api";
        
        public ProjectsControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        [Fact]
        public async Task CountTasksForProjectByUser_WhenProjectWithId1_ThenReturnResponseWithFirstValue1()
        {
            int userId = 1;
            int expectedResultEntitiesCount = 2;
            int expectedProjectId = 24;
            var httpResponse = await _client.GetAsync($"{BaseUrl}/projects/tasks/" + userId);

            var response = await httpResponse.Content.ReadAsStringAsync();
            var userProjectsTasks = JsonConvert.DeserializeObject<List<KeyValuePair<ProjectDTO, int>>>(response);

            Assert.Equal(expectedResultEntitiesCount, userProjectsTasks.Count);
            Assert.Equal(userId, userProjectsTasks.FirstOrDefault().Value);
            Assert.Equal(expectedProjectId, userProjectsTasks.FirstOrDefault().Key.Id);
        }

        [Fact]
        public async Task CreateProject_ThenResponseWithCode200AndCorrespondedBody()
        {
            var project = new ProjectDTO
            {
                Id = 110,
                Name = "Neque quia nam autem iure.",
                Description = "Eligendi voluptatem debitis. Ipsa quod et porro omnis et aut dolores. Ad aut qui sit. Esse amet error.",
                CreatedAt = new DateTime(2020, 07, 01, 01, 48, 27, 78),
                Deadline = new DateTime(2020, 10, 04, 02, 04, 02, 7),
                AuthorId = 1,
                TeamId = 9
            };

            string json = JsonConvert.SerializeObject(project);
            var httpResponse = await _client.PostAsync($"{BaseUrl}/projects", new StringContent(json, Encoding.UTF8, "application/json"));

            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var createdProject = JsonConvert.DeserializeObject<Project>(stringResponse);

            await _client.DeleteAsync($"{BaseUrl}/projects/" + project.Id);

            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
            Assert.Equal(project.Id, createdProject.Id);
            Assert.Equal(project.Name, createdProject.Name);
        }

        [Fact]
        public async Task CreateProject_WhenEmptyProject_ThenResponseCode400()
        {

            string json = JsonConvert.SerializeObject(null); ;
            var httpResponse = await _client.PostAsync($"{BaseUrl}/projects", new StringContent(json, Encoding.UTF8, "application/json"));
            Assert.Equal(HttpStatusCode.BadRequest, httpResponse.StatusCode);
        }
    }
}
