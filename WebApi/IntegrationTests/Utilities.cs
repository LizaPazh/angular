﻿using DAL;
using DAL.Context;
using System;
using System.Collections.Generic;
using System.Text;

namespace IntegrationTests
{
    public class Utilities
    {
        static readonly DataSeeder dataSeeder = new DataSeeder();
        public static void InitializeDbForTests(CompanyDBContext db)
        {
            db.Projects.AddRange(dataSeeder.LoadProjects());
            db.ProjectTasks.AddRange(dataSeeder.LoadTasks());
            db.Teams.AddRange(dataSeeder.LoadTeams());
            db.Users.AddRange(dataSeeder.LoadUsers());
            db.SaveChanges();
        }
    }
}
