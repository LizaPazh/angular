﻿using BLL.DTOs;
using Newtonsoft.Json;
using Project_Structure;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace IntegrationTests
{
    public class UsersControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;
        private const string BaseUrl = "http://localhost:44392/api";
        public UsersControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        [Fact]
        public async Task DeleteUser_ThenResponseCode204AndGetByIdResponse404()
        {
            var user = new UserDTO
            {
                Id = 51,
                FirstName = "Retha",
                LastName = "Will",
                Email = "Retha67@yahoo.com",
                Birthday = new DateTime(2007, 07, 01, 13, 59, 44, 16),
                RegisteredAt = "2020-06-01T07:00:16.6860452+00:00",
                TeamId = 1,
            };

            string json = JsonConvert.SerializeObject(user);
            await _client.PostAsync($"{BaseUrl}/users", new StringContent(json, Encoding.UTF8, "application/json"));

            var httpDeleteResponse = await _client.DeleteAsync($"{BaseUrl}/users/" + user.Id);

            Assert.Equal(HttpStatusCode.NoContent, httpDeleteResponse.StatusCode);

            var httpResponseGetById = await _client.GetAsync($"{BaseUrl}/users/" + user.Id);

            Assert.Equal(HttpStatusCode.NotFound, httpResponseGetById.StatusCode);
        }

        [Fact]
        public async Task DeleteUser_WhenUserDoesntExist_ThenResponseCode404()
        {
            int userId = 10000;
            var httpResponse = await _client.DeleteAsync($"{BaseUrl}/users" + userId);
            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);

        }
        [Fact]
        public async Task GetUsersCharacteristics_WhenUserWithId1_ThenReturnUserWithId1()
        {
            int userId = 1;
            var httpResponse = await _client.GetAsync($"{BaseUrl}/users/GetUsersCharacteristics/" + userId);

            var response = await httpResponse.Content.ReadAsStringAsync();
            var user = JsonConvert.DeserializeObject<UserWithParameters>(response);

            Assert.Equal(userId, user.User.Id);
            
        }

        [Fact]
        public async Task GetUsersCharacteristics_WhenUserWithIdDoesntExist_ThenExceptionThrown()
        {
            int userId = 10000;
            var httpResponse = await _client.GetAsync($"{BaseUrl}/users/GetUsersCharacteristics/" + userId);
            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);
        }
    }
}
