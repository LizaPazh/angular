﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Company.DLL.Migrations
{
    public partial class ChangeDateTimeToString : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "RegisteredAt",
                table: "Users",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                column: "RegisteredAt",
                value: "2020-06-24T09:11:10.5691173+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                column: "RegisteredAt",
                value: "2020-06-27T20:20:55.7940233+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                column: "RegisteredAt",
                value: "2020-06-22T12:35:42.5834106+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                column: "RegisteredAt",
                value: "2020-07-01T05:22:15.869556+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                column: "RegisteredAt",
                value: "2020-06-23T18:09:18.5038054+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                column: "RegisteredAt",
                value: "2020-06-01T23:17:26.4309832+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                column: "RegisteredAt",
                value: "2020-06-23T14:37:38.0498331+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                column: "RegisteredAt",
                value: "2020-05-28T17:02:02.295508+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                column: "RegisteredAt",
                value: "2020-06-22T19:42:43.764246+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                column: "RegisteredAt",
                value: "2020-06-02T23:16:31.267727+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                column: "RegisteredAt",
                value: "2020-06-29T13:40:13.993952+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                column: "RegisteredAt",
                value: "2020-06-11T22:12:48.7118931+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                column: "RegisteredAt",
                value: "2020-06-18T07:48:28.9542609+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                column: "RegisteredAt",
                value: "2020-06-03T14:20:09.6735222+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                column: "RegisteredAt",
                value: "2020-06-08T13:04:31.6144169+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                column: "RegisteredAt",
                value: "2020-07-01T07:00:16.6860452+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                column: "RegisteredAt",
                value: "2020-05-27T21:57:23.5397637+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                column: "RegisteredAt",
                value: "2020-05-22T17:21:56.0998576+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                column: "RegisteredAt",
                value: "2020-05-22T04:53:58.617495+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                column: "RegisteredAt",
                value: "2020-06-05T11:13:47.4415614+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                column: "RegisteredAt",
                value: "2020-06-13T20:22:28.4865957+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 22,
                column: "RegisteredAt",
                value: "2020-06-21T15:57:15.5372226+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 23,
                column: "RegisteredAt",
                value: "2020-06-21T22:43:12.7386212+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 24,
                column: "RegisteredAt",
                value: "2020-06-09T18:35:51.8445877+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 25,
                column: "RegisteredAt",
                value: "2020-06-17T00:38:36.7481173+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 26,
                column: "RegisteredAt",
                value: "2020-06-16T12:02:16.5611785+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 27,
                column: "RegisteredAt",
                value: "2020-06-12T19:59:15.8381128+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 28,
                column: "RegisteredAt",
                value: "2020-06-07T07:40:25.1336725+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 29,
                column: "RegisteredAt",
                value: "2020-06-22T03:11:14.5553721+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 30,
                column: "RegisteredAt",
                value: "2020-06-26T11:35:49.2600625+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 31,
                column: "RegisteredAt",
                value: "2020-06-23T22:02:32.0102632+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 32,
                column: "RegisteredAt",
                value: "2020-05-28T23:02:01.1048139+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 33,
                column: "RegisteredAt",
                value: "2020-06-16T23:08:50.8227785+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 34,
                column: "RegisteredAt",
                value: "2020-05-20T22:32:22.6670028+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 35,
                column: "RegisteredAt",
                value: "2020-05-25T00:12:46.6373168+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 36,
                column: "RegisteredAt",
                value: "2020-06-13T06:50:19.5179342+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 37,
                column: "RegisteredAt",
                value: "2020-06-11T16:20:36.9918256+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 38,
                column: "RegisteredAt",
                value: "2020-05-31T07:55:57.6050218+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 39,
                column: "RegisteredAt",
                value: "2020-06-20T03:11:11.9245305+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 40,
                column: "RegisteredAt",
                value: "2020-06-26T12:13:46.9100765+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 41,
                column: "RegisteredAt",
                value: "2020-06-25T16:11:58.8758216+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 42,
                column: "RegisteredAt",
                value: "2020-05-28T02:19:58.3211503+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 43,
                column: "RegisteredAt",
                value: "2020-05-22T10:11:03.0933909+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 44,
                column: "RegisteredAt",
                value: "2020-06-22T11:59:33.6871371+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 45,
                column: "RegisteredAt",
                value: "2020-06-13T08:07:49.1606262+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 46,
                column: "RegisteredAt",
                value: "2020-05-26T19:39:37.5032854+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 47,
                column: "RegisteredAt",
                value: "2020-06-01T10:32:48.5838304+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 48,
                column: "RegisteredAt",
                value: "2020-05-23T14:21:58.2044981+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 49,
                column: "RegisteredAt",
                value: "2020-06-03T04:13:47.0361354+00:00");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 50,
                column: "RegisteredAt",
                value: "2020-06-01T16:41:43.9018228+00:00");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "RegisteredAt",
                table: "Users",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                column: "RegisteredAt",
                value: new DateTime(2020, 6, 24, 12, 11, 10, 569, DateTimeKind.Local).AddTicks(1173));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                column: "RegisteredAt",
                value: new DateTime(2020, 6, 27, 23, 20, 55, 794, DateTimeKind.Local).AddTicks(233));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                column: "RegisteredAt",
                value: new DateTime(2020, 6, 22, 15, 35, 42, 583, DateTimeKind.Local).AddTicks(4106));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                column: "RegisteredAt",
                value: new DateTime(2020, 7, 1, 8, 22, 15, 869, DateTimeKind.Local).AddTicks(5560));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                column: "RegisteredAt",
                value: new DateTime(2020, 6, 23, 21, 9, 18, 503, DateTimeKind.Local).AddTicks(8054));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                column: "RegisteredAt",
                value: new DateTime(2020, 6, 2, 2, 17, 26, 430, DateTimeKind.Local).AddTicks(9832));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                column: "RegisteredAt",
                value: new DateTime(2020, 6, 23, 17, 37, 38, 49, DateTimeKind.Local).AddTicks(8331));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                column: "RegisteredAt",
                value: new DateTime(2020, 5, 28, 20, 2, 2, 295, DateTimeKind.Local).AddTicks(5080));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                column: "RegisteredAt",
                value: new DateTime(2020, 6, 22, 22, 42, 43, 764, DateTimeKind.Local).AddTicks(2460));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                column: "RegisteredAt",
                value: new DateTime(2020, 6, 3, 2, 16, 31, 267, DateTimeKind.Local).AddTicks(7270));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                column: "RegisteredAt",
                value: new DateTime(2020, 6, 29, 16, 40, 13, 993, DateTimeKind.Local).AddTicks(9520));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                column: "RegisteredAt",
                value: new DateTime(2020, 6, 12, 1, 12, 48, 711, DateTimeKind.Local).AddTicks(8931));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                column: "RegisteredAt",
                value: new DateTime(2020, 6, 18, 10, 48, 28, 954, DateTimeKind.Local).AddTicks(2609));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                column: "RegisteredAt",
                value: new DateTime(2020, 6, 3, 17, 20, 9, 673, DateTimeKind.Local).AddTicks(5222));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                column: "RegisteredAt",
                value: new DateTime(2020, 6, 8, 16, 4, 31, 614, DateTimeKind.Local).AddTicks(4169));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                column: "RegisteredAt",
                value: new DateTime(2020, 7, 1, 10, 0, 16, 686, DateTimeKind.Local).AddTicks(452));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                column: "RegisteredAt",
                value: new DateTime(2020, 5, 28, 0, 57, 23, 539, DateTimeKind.Local).AddTicks(7637));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                column: "RegisteredAt",
                value: new DateTime(2020, 5, 22, 20, 21, 56, 99, DateTimeKind.Local).AddTicks(8576));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                column: "RegisteredAt",
                value: new DateTime(2020, 5, 22, 7, 53, 58, 617, DateTimeKind.Local).AddTicks(4950));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                column: "RegisteredAt",
                value: new DateTime(2020, 6, 5, 14, 13, 47, 441, DateTimeKind.Local).AddTicks(5614));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                column: "RegisteredAt",
                value: new DateTime(2020, 6, 13, 23, 22, 28, 486, DateTimeKind.Local).AddTicks(5957));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 22,
                column: "RegisteredAt",
                value: new DateTime(2020, 6, 21, 18, 57, 15, 537, DateTimeKind.Local).AddTicks(2226));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 23,
                column: "RegisteredAt",
                value: new DateTime(2020, 6, 22, 1, 43, 12, 738, DateTimeKind.Local).AddTicks(6212));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 24,
                column: "RegisteredAt",
                value: new DateTime(2020, 6, 9, 21, 35, 51, 844, DateTimeKind.Local).AddTicks(5877));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 25,
                column: "RegisteredAt",
                value: new DateTime(2020, 6, 17, 3, 38, 36, 748, DateTimeKind.Local).AddTicks(1173));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 26,
                column: "RegisteredAt",
                value: new DateTime(2020, 6, 16, 15, 2, 16, 561, DateTimeKind.Local).AddTicks(1785));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 27,
                column: "RegisteredAt",
                value: new DateTime(2020, 6, 12, 22, 59, 15, 838, DateTimeKind.Local).AddTicks(1128));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 28,
                column: "RegisteredAt",
                value: new DateTime(2020, 6, 7, 10, 40, 25, 133, DateTimeKind.Local).AddTicks(6725));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 29,
                column: "RegisteredAt",
                value: new DateTime(2020, 6, 22, 6, 11, 14, 555, DateTimeKind.Local).AddTicks(3721));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 30,
                column: "RegisteredAt",
                value: new DateTime(2020, 6, 26, 14, 35, 49, 260, DateTimeKind.Local).AddTicks(625));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 31,
                column: "RegisteredAt",
                value: new DateTime(2020, 6, 24, 1, 2, 32, 10, DateTimeKind.Local).AddTicks(2632));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 32,
                column: "RegisteredAt",
                value: new DateTime(2020, 5, 29, 2, 2, 1, 104, DateTimeKind.Local).AddTicks(8139));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 33,
                column: "RegisteredAt",
                value: new DateTime(2020, 6, 17, 2, 8, 50, 822, DateTimeKind.Local).AddTicks(7785));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 34,
                column: "RegisteredAt",
                value: new DateTime(2020, 5, 21, 1, 32, 22, 667, DateTimeKind.Local).AddTicks(28));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 35,
                column: "RegisteredAt",
                value: new DateTime(2020, 5, 25, 3, 12, 46, 637, DateTimeKind.Local).AddTicks(3168));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 36,
                column: "RegisteredAt",
                value: new DateTime(2020, 6, 13, 9, 50, 19, 517, DateTimeKind.Local).AddTicks(9342));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 37,
                column: "RegisteredAt",
                value: new DateTime(2020, 6, 11, 19, 20, 36, 991, DateTimeKind.Local).AddTicks(8256));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 38,
                column: "RegisteredAt",
                value: new DateTime(2020, 5, 31, 10, 55, 57, 605, DateTimeKind.Local).AddTicks(218));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 39,
                column: "RegisteredAt",
                value: new DateTime(2020, 6, 20, 6, 11, 11, 924, DateTimeKind.Local).AddTicks(5305));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 40,
                column: "RegisteredAt",
                value: new DateTime(2020, 6, 26, 15, 13, 46, 910, DateTimeKind.Local).AddTicks(765));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 41,
                column: "RegisteredAt",
                value: new DateTime(2020, 6, 25, 19, 11, 58, 875, DateTimeKind.Local).AddTicks(8216));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 42,
                column: "RegisteredAt",
                value: new DateTime(2020, 5, 28, 5, 19, 58, 321, DateTimeKind.Local).AddTicks(1503));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 43,
                column: "RegisteredAt",
                value: new DateTime(2020, 5, 22, 13, 11, 3, 93, DateTimeKind.Local).AddTicks(3909));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 44,
                column: "RegisteredAt",
                value: new DateTime(2020, 6, 22, 14, 59, 33, 687, DateTimeKind.Local).AddTicks(1371));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 45,
                column: "RegisteredAt",
                value: new DateTime(2020, 6, 13, 11, 7, 49, 160, DateTimeKind.Local).AddTicks(6262));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 46,
                column: "RegisteredAt",
                value: new DateTime(2020, 5, 26, 22, 39, 37, 503, DateTimeKind.Local).AddTicks(2854));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 47,
                column: "RegisteredAt",
                value: new DateTime(2020, 6, 1, 13, 32, 48, 583, DateTimeKind.Local).AddTicks(8304));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 48,
                column: "RegisteredAt",
                value: new DateTime(2020, 5, 23, 17, 21, 58, 204, DateTimeKind.Local).AddTicks(4981));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 49,
                column: "RegisteredAt",
                value: new DateTime(2020, 6, 3, 7, 13, 47, 36, DateTimeKind.Local).AddTicks(1354));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 50,
                column: "RegisteredAt",
                value: new DateTime(2020, 6, 1, 19, 41, 43, 901, DateTimeKind.Local).AddTicks(8228));
        }
    }
}
