﻿using DAL.Context;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace DAL.Repositories
{
    public interface IProjectRepository : IRepository<Project>
    {
    }
    public class ProjectRepository : RepositoryBase<Project>, IProjectRepository
    {
        public ProjectRepository(CompanyDBContext context)
              : base(context)
        { }
       
    }
}
