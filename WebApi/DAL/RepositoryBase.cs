﻿using DAL.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DAL
{
    public class RepositoryBase<T> : IRepository<T> where T : class
    {
        protected readonly CompanyDBContext context;
        public RepositoryBase(CompanyDBContext context)
        {
            this.context = context;
        }
        public async Task Create(T entity)
        {
            context.Add(entity);
            await context.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
            var entity = context.Set<T>().Find(id);

            if (entity != null)
            {
                context.Remove(entity);
            }
            await context.SaveChangesAsync();
        }
        public async Task<IEnumerable<T>> GetAll(params Expression<Func<T, object>>[] properties)
        {
            var query = context.Set<T>() as IQueryable<T>;

            if (properties == null)
                return query.AsNoTracking();

            return await properties.Aggregate(query, (current, property) => current.Include(property)).ToListAsync();
        }

        public async Task<T> GetById(int id)
        {
            var entity  = await context.Set<T>().FindAsync(id);
            context.Entry(entity).State = EntityState.Detached;
            return entity;
        }

        public async Task Update(T entity)
        {
            context.Update(entity);
            await context.SaveChangesAsync();
        }
    }
}
