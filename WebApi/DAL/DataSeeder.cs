﻿using DAL.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DAL
{
    public class DataSeeder
    {
        public IEnumerable<Project> LoadProjects()
        {     
            var fileName = "Data\\projects.json";
            using (StreamReader file = new StreamReader(fileName))
            {
                return JsonConvert.DeserializeObject<IEnumerable<Project>>(file.ReadToEnd());
            }
        }

        public  IEnumerable<ProjectTask> LoadTasks()
        {
            var fileName = "Data\\tasks.json";
            using (StreamReader file = new StreamReader(fileName))
            {
                return JsonConvert.DeserializeObject<IEnumerable<ProjectTask>>(file.ReadToEnd());
            }
        }

        public  IEnumerable<Team> LoadTeams()
        {
            var fileName = "Data\\teams.json";
            using (StreamReader file = new StreamReader(fileName))
            {
                return JsonConvert.DeserializeObject<IEnumerable<Team>>(file.ReadToEnd());
            }
        }

        public IEnumerable<User> LoadUsers()
        {
            var fileName = "Data\\users.json";
            using (StreamReader file = new StreamReader(fileName))
            {
                return JsonConvert.DeserializeObject<IEnumerable<User>>(file.ReadToEnd());
            }
        }
    }
}
