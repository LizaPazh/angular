﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.DTOs;
using BLL.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Project_Structure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService service;
        public UsersController(IUserService teamService)
        {
            service = teamService;
        }
        [HttpGet]
        public async Task<IActionResult> GetUsers()
        {
            return Ok(await service.GetAllUsers());
        }

        [HttpGet("{userId}")]
        public async Task<IActionResult> GetUserById(int userId)
        {
            var user = await service.GetUserById(userId);

            return Ok(user);
        }

        [HttpPost]
        public async Task<IActionResult> CreateUser(UserDTO user)
        {
            await service.CreateUser(user);
            return Ok();
        }

        [HttpPut]
        public async Task<IActionResult> UpdateUser(UserDTO user)
        {
            await service.UpdateUser(user);
            return NoContent();
        }

        [HttpDelete("{userId}")]
        public async Task<IActionResult> DeleteUser(int userId)
        {
            await service.DeleteUser(userId);
            return NoContent();
        }

        [HttpGet("usersByABCWithSortedTasks")]
        public async Task<IActionResult> UsersByABCWithTasksSortedByName()
        {
            var users = await service.UsersByABCWithTasksSortedByName();
            return Ok(users);
        }

        [HttpGet("GetUsersCharacteristics/{userId}")]
        public async Task<IActionResult> GetUsersCharacteristics(int userId)
        {
            var userWithParameters = await service.GetUsersCharacteristics(userId);
            return Ok(userWithParameters);
        }
    }
}