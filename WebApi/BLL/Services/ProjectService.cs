﻿using AutoMapper;
using BLL.DTOs;
using BLL.Exceptions;
using BLL.Interfaces;
using DAL.Models;
using DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class ProjectService: IProjectService
    {
        private readonly IProjectRepository _rep;
        private readonly IMapper _mapper;
        public ProjectService(IProjectRepository rep, IMapper mapper)
        {
            _rep = rep;
            _mapper = mapper;
        }
        public async Task<ProjectDTO> GetProjectById(int projectId)
        {
            var project = await _rep.GetById(projectId);

            if (project == null)
            {
                throw new NotFoundException($"Project with id: {projectId} cannot be found");
            }

            return _mapper.Map<ProjectDTO>(project);
        }
        public async Task<IEnumerable<ProjectDTO>> GetAllProjects()
        {
            var projects =  await _rep.GetAll();
            return _mapper.Map<IEnumerable<ProjectDTO>>(projects);
        }
        public async Task CreateProject(ProjectDTO projectDTO)
        {
            if (projectDTO == null)
            {
                throw new ArgumentNullException();
            }

            var project = _mapper.Map<Project>(projectDTO);
            await _rep.Create(project);
        }
        public async Task UpdateProject(ProjectDTO projectDTO)
        {
            if (projectDTO == null)
            {
                throw new ArgumentNullException();
            }
            var project = _mapper.Map<Project>(projectDTO);
            await _rep.Update(project);
        }
        public async Task DeleteProject(int projectId)
        {
            var project = await _rep.GetById(projectId);

            if (project == null)
            {
                throw new NotFoundException($"Project with id: {projectId} cannot be found");
            }

            await _rep.Delete(projectId);
        }
        public async Task<Dictionary<ProjectDTO, int>> CountTasksForProjectByUser(int userId)
        {
            var dictionaries = (await _rep.GetAll(p => p.ProjectTasks)).Where(p => p.AuthorId == userId)
                          .ToDictionary(y => y, y => y.ProjectTasks.Count());

            return _mapper.Map<Dictionary<ProjectDTO, int>>(dictionaries);
        }
        public async Task<IEnumerable<ProjectWithParameters>> GetProjectWithCharacteristics()
        {
            return (await _rep.GetAll(p => p.ProjectTasks, p=>p.Team.Users)).Select(p => new ProjectWithParameters
            {
                Project = _mapper.Map<ProjectDTO>(p),
                TaskByDescription = _mapper.Map<TaskDTO>(p.ProjectTasks.OrderByDescending(t => t.Description.Length).FirstOrDefault()),
                TaskByName = _mapper.Map<TaskDTO>(p.ProjectTasks.OrderBy(t => t.Name.Length).FirstOrDefault()),
                UsersCount = (p.Description.Length > 20 || p.ProjectTasks.Count() < 3) ? p.Team.Users.Count() : 0
            });
        }
    }
}
