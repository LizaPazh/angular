﻿using AutoMapper;
using BLL.DTOs;
using BLL.Exceptions;
using BLL.Interfaces;
using DAL.Models;
using DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class TeamService : ITeamService
    {
        private readonly ITeamRepository _rep;
        private readonly IMapper _mapper;
        public TeamService(ITeamRepository rep, IMapper mapper)
        {
            _rep = rep;
            _mapper = mapper;
        }
        public async Task<TeamDTO> GetTeamById(int teamId)
        {
            var team = await _rep.GetById(teamId);

            if (team == null)
            {
                throw new NotFoundException($"Team with id: {teamId} not found");
            }

            return _mapper.Map<TeamDTO>(team);
        }
        public async Task<IEnumerable<TeamDTO>> GetAllTeams()
        {
            return _mapper.Map<IEnumerable<TeamDTO>>(await _rep.GetAll());
        }
        public async Task CreateTeam(TeamDTO teamDTO)
        {
            if (teamDTO == null)
            {
                throw new ArgumentNullException();
            }
            var team = _mapper.Map<Team>(teamDTO);
            await _rep.Create(team);
        }
        public async Task UpdateTeam(TeamDTO teamDTO)
        {
            var existingTeam = await _rep.GetById(teamDTO.Id);

            if (existingTeam == null)
            {
                throw new NotFoundException($"Team with {teamDTO.Id} not found");
            }
            var team = _mapper.Map<Team>(teamDTO);
            await _rep.Update(team);
        }
        public async Task DeleteTeam(int teamId)
        {
            var team = await _rep.GetById(teamId);
            if (team == null)
            {
                throw new NotFoundException($"Team with id: {teamId} not found");
            }
            await _rep.Delete(teamId);
        }
        public async Task<IEnumerable<TeamWithUsers>> GetTeamsByUserAge()
        {
            return (await _rep.GetAll(t=>t.Users)).Where(t => t.Users.Any() && t.Users.All(u => DateTime.Now.Year - u.Birthday.Year >= 10))
                        .AsEnumerable()
                        .Select(t => { t.Users = t.Users.OrderByDescending(u => u.RegisteredAt); return t; })
                        .GroupBy(t => t)
                        .Select(g => new TeamWithUsers{
                            TeamId = g.Key.Id,
                            TeamName = g.Key.Name,
                            Users = _mapper.Map<IEnumerable<UserDTO>>(g.Key.Users)
                        });

        }
    }
}
