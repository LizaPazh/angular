﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.DTOs
{
    public class ProjectWithParameters
    {
        public ProjectDTO Project { get; set; }
        public TaskDTO TaskByDescription { get; set; }
        public TaskDTO TaskByName { get; set; }
        public int UsersCount { get; set; }
    }
}
