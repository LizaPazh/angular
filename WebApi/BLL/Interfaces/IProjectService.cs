﻿using BLL.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IProjectService
    {
        Task<ProjectDTO> GetProjectById(int projectId);
        Task<IEnumerable<ProjectDTO>> GetAllProjects();
        Task CreateProject(ProjectDTO project);
        Task UpdateProject(ProjectDTO project);
        Task DeleteProject(int projectId);
        Task<Dictionary<ProjectDTO, int>> CountTasksForProjectByUser(int userId);
        Task<IEnumerable<ProjectWithParameters>> GetProjectWithCharacteristics();
    }
}
