﻿using BLL.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IUserService
    {
        Task<UserDTO> GetUserById(int userId);
        Task<IEnumerable<UserDTO>> GetAllUsers();
        Task CreateUser(UserDTO user);
        Task UpdateUser(UserDTO user);
        Task DeleteUser(int userId);
        Task<IEnumerable<UserDTO>> UsersByABCWithTasksSortedByName();
        Task<UserWithParameters> GetUsersCharacteristics(int userId);
    }
}
