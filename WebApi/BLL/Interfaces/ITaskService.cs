﻿using BLL.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface ITaskService
    {
        Task<TaskDTO> GetTaskById(int taskId);
        Task<IEnumerable<TaskDTO>> GetAllTasks();
        Task CreateTask(TaskDTO task);
        Task UpdateTask(TaskDTO task);
        Task DeleteTask(int taskId);
        Task<IEnumerable<TaskDTO>> TasksByUser(int userId);

        Task<IEnumerable<Tuple<int, string>>> FinishedTasksByUser(int userId);

        Task<IEnumerable<TaskDTO>> UnFinishedTasksByUser(int userId);
    }
}
